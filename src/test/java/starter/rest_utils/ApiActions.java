package starter.rest_utils;

import net.thucydides.core.util.EnvironmentVariables;
import io.restassured.module.jsv.JsonSchemaValidator;

import static net.serenitybdd.rest.SerenityRest.then;

public class ApiActions {

    private EnvironmentVariables environmentVariables;

    public String getUrl(){
        return this.environmentVariables.optionalProperty("environment.default.baseUrl").toString()
                + this.environmentVariables.optionalProperty("environment.default.uri").toString();
    }

    public int getStatusCode(){
        return then().extract().statusCode();
    }
}
