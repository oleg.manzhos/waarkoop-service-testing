package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.module.jsv.JsonSchemaValidator;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import starter.rest_utils.ApiActions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class SearchStepDefinitions {

    @Steps
    public ApiActions apiActions;

    @When("{actor} searches for the <product> product")
    public void actorCallsEndpoint(Actor actor, String product) {
        SerenityRest.given().log().all()
                .get(apiActions.getUrl(), product);
    }

    @Then("The response code is {int}")
    public void actorSeesTheResultsDisplayedForApple(int responseCode) {
            Assert.assertEquals(responseCode, apiActions.getStatusCode());
    }

    @Then("Error message with (.*) text is received")
    public void actorReceivesErrorMessage(String errorMessage) {
        restAssuredThat(response -> response.body("error", equalTo(errorMessage)));
    }

    @Then("^The response schema is (.*)$")
    public void assertValidJsonResponse(String jsonSchema){
        restAssuredThat(response -> response.body(JsonSchemaValidator.matchesJsonSchemaInClasspath("/json_schemas/"+jsonSchema)));

    }
}
