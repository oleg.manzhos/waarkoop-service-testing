Feature: In case a product doesn't exist, or the search term is invalid, the result should clearly state about this
  @unhappy_path
  Scenario Outline:

    When Tester searches for the <product> product
    Then Error message with "Not found" text is received

    Examples:
    |product|status_code|
    |Apple  |404        |
    |Cola   |404        |
    |

