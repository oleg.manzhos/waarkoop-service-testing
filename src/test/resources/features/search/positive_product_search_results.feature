Feature: For the apple, orange, cola and paste products, the response should contain information about providers and products

  @happy_path
  Scenario Outline: For the list of available products the result is returned
    When Tester searches for the <product> product
    Then The response code is 200
    And The response schema is existing_product_response_schema.json

    Examples:
    | product |
    | orange  |
    | apple   |
    | pasta   |
    | cola    |

